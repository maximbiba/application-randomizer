package service;
import memory.GenerateRandom;

import java.util.ArrayList;
import java.util.Scanner;


public class CommandService {
    private static final String CMD_EXIT = "exit";
    private static final String CMD_GENERATE = "generate";
    private static final String CMD_HELP = "help";
    private static final String CMD_SET_BOUNDARIES = "setBoundaries";
    private static final String CMD_SHOW_BOUNDARIES = "showBoundaries";
    private final Scanner scanner = new Scanner(System.in);

    private ArrayList<Integer> excludeList = new ArrayList<Integer>(1);
    private GenerateRandom searchPool = new GenerateRandom(1,1,excludeList);
    private final String MSG_RESET = "Boundaries are reset at previous";
    public CommandService(){
        System.out.println("Enter: help - to see list of commands");
        getNextCommand();

    }
    private void processCommand(String command){
        switch (command){
            case CMD_EXIT:
                System.out.println("Bye!");
                System.exit(0);
            case CMD_HELP:
                help();
                getNextCommand();
                break;
            case CMD_SHOW_BOUNDARIES:
                showBoundaries();
                getNextCommand();
                break;
            case CMD_SET_BOUNDARIES:
                boundaries();
                getNextCommand();
                break;
            case CMD_GENERATE:
                generate();
                getNextCommand();
                break;
            default:
                System.out.println("Invalid command");
                getNextCommand();
                break;
        }
    }
    private void help(){
        System.out.println("List of commands:\n showBoundaries \n setBoundaries \n generate \n help \n exit ");
    }

    private void showBoundaries(){
        System.out.println("The boundaries are set at "+searchPool.getStart()+ " and "+searchPool.getEnd()+".");

    }

    private void boundaries(){
        System.out.println("Set new boundaries");
        excludeList.clear();

        int tempEnd = searchPool.getEnd();
        int tempStart = searchPool.getStart();

        try{
            System.out.println("Set START line");
            searchPool.setStart(scanner.nextInt());
            int start = searchPool.getStart();
            if(0 < start & start<201){
                searchPool.setStart(start);

            }else{
                throw new Exception();
            }

            System.out.println("Set END line");
            searchPool.setEnd(scanner.nextInt());
            int end = searchPool.getEnd();
            if(0< end & end<201){
                searchPool.setEnd(end);
            }else{
                throw new Exception();
            }

            if(searchPool.getStart()>searchPool.getEnd()){
                throw new Exception();
            }
        }
        catch (Exception e){
            System.out.println("-----------------------------------");
            System.out.println("Boundaries must be set as INT type");
            System.out.println("START line < END line");
            System.out.println("START must be equal or greater than 1");
            System.out.println("END must be equal or less than 200");
            System.out.println("-----------------------------------"+'\n');

            searchPool.setStart(tempStart);
            searchPool.setEnd(tempEnd);

            System.out.println(MSG_RESET);

        }
        System.out.println("New boundaries are set at "+searchPool.getStart()+" and "+searchPool.getEnd()+".");
    }

    private void getNextCommand(){
        String nextCommand = scanner.next();
        processCommand(nextCommand);
    }
    private void generate(){

        if(excludeList.size()==searchPool.getEnd()-searchPool.getStart()+1){
            System.out.println("Running out of numbers, please select new boundaries");

        }else{
            int rnd = searchPool.getNextRandom();
            System.out.println(searchPool.excludeList);
            System.out.println(rnd);

        }
    }


}



