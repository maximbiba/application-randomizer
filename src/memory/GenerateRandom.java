package memory;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;


public class GenerateRandom {
    int start;
    int end;
    public ArrayList<Integer> excludeList;

    public GenerateRandom(int start, int end, ArrayList<Integer>excludeList) {
        this.start = start;
        this.end = end;
        this.excludeList = excludeList;
    }

    public int getStart(){
        return start;
    }
    public void setStart(int start) {
        this.start = start;
    }
    public int getEnd(){
        return end;
    }
    public void setEnd(int end){
        this.end = end;
    }
    public int getNextRandom()throws IllegalArgumentException{
        Random rnd = new Random();
        int random = start + rnd.nextInt(end - start + 1 - excludeList.size());

        for (int ex : excludeList) {
            if (random < ex){
                break;
            }
            random++;
        }
        excludeList.add(random);
        Collections.sort(excludeList);
        return random;
    }

}
